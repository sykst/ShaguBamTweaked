# ShaguBamTweaked

Everytime a new crit record has be done, this addon shows up the new record and plays a bruce lee soundfile.
It can be configured to notify via emotes, group chat, raid chat, say, yell or siltently for the player only.
By default it will use emotes. To configure, please type: /bam or /shagubam

![screenshot](https://gitlab.com/sykst/ShaguBamTweaked/raw/master/img/screenshot.jpg)

## Tweak

![permission](https://gitlab.com/sykst/ShaguBamTweaked/raw/master/img/permission.png)

### `/crit` or `/critical`

Prints the list of all your highest crits.

![crit](https://gitlab.com/sykst/ShaguBamTweaked/raw/master/img/crit.png)

## Installation
1. Download **[Latest Version](https://gitlab.com/sykst/ShaguBamTweaked/-/archive/master/ShaguBamTweaked-master.zip)**
2. Unpack the Zip file
3. Rename the folder "ShaguBamTweaked-master" to "ShaguBamTweaked"
4. Copy "ShaguBamTweaked" into Wow-Directory\Interface\AddOns
5. Restart Wow